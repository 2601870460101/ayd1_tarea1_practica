from flask import Flask, request,  jsonify
from flask_mysqldb import MySQL
from jinja2 import Undefined
from botocore.exceptions import ClientError
from flask_cors import CORS

# initializations
app = Flask(__name__)
CORS(app)

mysql = MySQL(app)

@app.route('/', methods=['GET'])
def Index():
    return "hola"

#================================================================================  USUARIOS  =========================================================================

@app.route('/suma/<v1>/<v2>', methods=['POST'])
def suma(v1,v2): 
    if request.method == 'POST':  
        try:
            suma = int(v1) + int(v2)
            
            respuesta={
                    "suma": suma,
                    "status": 200
                }
            return respuesta
        except Exception as e:
            respuesta={
                    "message": "Error en la suma",
                    "status": 404
                }
            return respuesta
             


if __name__ == "__main__":
    app.run(host  = '0.0.0.0',port=4000)
    #app.run(port = 5004, debug=True)
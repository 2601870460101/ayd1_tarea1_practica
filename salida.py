from flask import Flask, request,  jsonify
from flask_mysqldb import MySQL
from jinja2 import Undefined
from botocore.exceptions import ClientError
from flask_cors import CORS

# initializations
app = Flask(__name__)
CORS(app)

mysql = MySQL(app)

@app.route('/', methods=['GET'])
def Index():
    return "hola"

#================================================================================  USUARIOS  =========================================================================

@app.route('/resta/<v1>/<v2>', methods=['POST'])
def resta(v1,v2): 
    if request.method == 'POST':  
        try:
            suma = int(v1) - int(v2)
            
            respuesta={
                    "resta": suma,
                    "status": 200
                }
            return respuesta
        except Exception as e:
            respuesta={
                    "message": "Error en la resta",
                    "status": 404
                }
            return respuesta


@app.route('/multi/<v1>/<v2>', methods=['POST'])
def multi(v1,v2): 
    if request.method == 'POST':  
        try:
            suma = int(v1) * int(v2)
            
            respuesta={
                    "multiplicacion": suma,
                    "status": 200
                }
            return respuesta
        except Exception as e:
            respuesta={
                    "message": "Error en la multiplicacion",
                    "status": 404
                }
            return respuesta


if __name__ == "__main__":
    app.run(host  = '0.0.0.0',port=3000)
    #app.run(port = 5004, debug=True)